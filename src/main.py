import yaml

if __name__ == "__main__":
    ## Load config file
    with open('/input/config.yaml') as config_file:
        task_config = yaml.load(config_file, Loader=yaml.FullLoader)

    ## Output "data" file by exporting the input task configuration
    with open('/output/eos.yaml', 'w') as eos_file:
        yaml.dump(task_config, eos_file)

    ## Output status
    status = {
        'code': 200,
        'message': f'''{task_config['module_name']} complete. The input config file was copied to "/output/eos.yaml".'''
    }
    with open('/output/status.yaml', 'w') as status_file:
        yaml.dump(status, status_file)
