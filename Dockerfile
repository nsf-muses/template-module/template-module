FROM python:alpine3.17

ARG UID=${UID:-1000}
ARG USERNAME=${USERNAME:-worker}

RUN mkdir -p /src /input /output && chown ${UID} /src /input /output

## Create unprivileged user
RUN adduser -u ${UID} -D ${USERNAME}
USER ${USERNAME}

WORKDIR /home/${USERNAME}
COPY ./src/requirements.txt .
RUN pip install --user -r requirements.txt

## Copy in Python source code
COPY --chown=${USERNAME}:${USERNAME} ./src/ /src/

CMD ["python", "/src/main.py"]
