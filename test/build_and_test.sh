#!/bin/bash

set -ex

## Change to parent directory of this script
cd "$(dirname "$(readlink -f "$0")")"/..

docker build . -t module_test:dev

TMP_DIR=$(mktemp -d)
mkdir -p "${TMP_DIR}"/{input,output}

cp test/inputs/test_config.1.yaml "${TMP_DIR}/input/"
cp test/inputs/test_config.2.yaml "${TMP_DIR}/input/"

echo "Running successful test:"

docker run --rm \
    -v "${TMP_DIR}/input/test_config.1.yaml":/input/config.yaml:ro \
    -v "${TMP_DIR}/output":/output \
    module_test:dev

find "${TMP_DIR}/output"
cat "${TMP_DIR}/output/status.yaml"
cat "${TMP_DIR}/output/eos.yaml"

echo "Running failure test:"

docker run --rm \
    -v "${TMP_DIR}/input/test_config.2.yaml":/input/config.yaml:ro \
    -v "${TMP_DIR}/output":/output \
    module_test:dev

find "${TMP_DIR}/output"
cat "${TMP_DIR}/output/status.yaml"
cat "${TMP_DIR}/output/eos.yaml"
