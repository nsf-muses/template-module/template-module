# Documentation

This folder contains the documentation source files for the module. It is self-contained for this module, but it is also designed for compilation into [the central documentation of the MUSES framework](https://musesframework.io/docs/).


## Local Sphinx build and website preview

To build and preview your documentation using Sphinx, run the build script to build and launch a Docker container like so:

```bash
bash docs/build/build.sh
```

Visit http://localhost:4000/docs/ in your browser to view the rendered website.
